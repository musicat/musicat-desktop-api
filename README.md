# MusiCat API Server
Colaboradores

Saara Gonzalez
Regina Saavedra


Comparación de tecnologías

| Herramienta | Características | ¿Por qué elegirlo? |
| ------ | ------ | ------------ |
| GitHub | Herramienta de control de cambios que permite activar devops y conectarse a muchas más aplicaciones para complementar funcionalidades | Compatibilidad con otras aplicaciones, comunidad de apoyo grande y bastante apoyo en la red. |
| Azure devops | Herramienta de devops, permite la creación de pipelines, pero se limita a un número de proyectos para la versión libre. | Aprendimos la herramienta en clase, cuenta con una Wiki e integraciones para gestionar los cambios de documentación y hacer un track de los requerimientos al código. |
| GitLab | Herramienta de devops que comparte características con azure devops como la creación de wiki y pipelines, además de la funcionalidad de ser el repositorio del proyecto | Uso de la herramienta en otros proyectos, integración de manera sencilla con Android studio, muestra gráfica de commits y merge realizados a la master. |


Selección de herramienta Gitlab.
Permite generar pipelines y wikis necesarias para el proyecto de la materia. Comparte características con azure devops, la cuál vimos en clase, y al menos un miembro del equipo está familiarizado con el uso de la herramienta y lleva usándola al menos un año.

Estrategia de uso de la herramienta
Se utilizará método ágil, lo que implica trabajar por sprints. Cada sprint será una rama a partir de la master que se creará al inicio de cada iteración y al final se unirá de nuevo a la master. Durante el tiempo de vida de la rama del sprint, se crearán ramas para la creación de las funcionalidades, mismas que al ser terminadas pasarán a unirse de nuevo a la rama de sprint.

Requerimientos funcionales

1. RF-01 El sistema movil deberá reproducir música por streaming
2. RF-02 El sistema movil deberá permitir al usuario iniciar sesión con su cuenta previamente registrada.
3. RF-03 El sistema movil deberá mostrar una lista de los albums que existen en el sistema.
4. RF-04 El sistema movil deberá mostrar la lista de canciones de los albums guardados en el sistema.
5. RF-05 El sistema deberá mostrar la imagen del perfil del usuario y su nombre de usuario con los que inició sesión.
6. RF-06 El sistema deberá permitir al usuario cerrar sesión en el sistema.
7. RF-07 El sistema deberá obtener las cuentas de google registradas en el dispositivo.
8. RF-08 El sistema deberá mostrar un reproductor al seleccionar una canción que cuenta con las opciones de pausar, iniciar, canción anterior y canción siguiente.
9. RF-09 El sistema deberá guardar en la base de datos los datos de cada usuario que se registra.


Link del documento:
https://1drv.ms/w/s!Ag-G45zwp1-lildvEoOmq494ez6E?e=6uNbuD

